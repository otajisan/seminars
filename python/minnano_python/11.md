みんなのPython勉強会

ハッシュタグ > #stapy

# #11

PyCon JP 2016 -> 2016/09/21(水)、22(木(祝日))

[資料](http://startpython.connpass.com/event/28359/)

4/12@半蔵門

##Talk 1:「私のPython学習奮闘記#5　〜学習のTIPs編〜」

-> 阿久津さんご欠席

##Talk 2:「春からはじめるPython環境の作り方」
辻さん(@tsjshg)

###Pythonの環境をどう作るか


####おすすめ
conda

おすすめ：Anaconda

スモールだとMiniconda

####実装の種類
- Cpython
- Jython
- IronPython
- Cython

####pip
pip -> python3.4から標準装備


####conda
Continuum Analytics社

```
$ conda install django
```

####pyenv

複数のバージョンのPythonを手軽に管理できる

####pyvenv

python3.3から標準装備
1つのPython環境に複数の仮想環境を作れる
インストールするパッケージを分けられる
同じことができるvirtualenvもある

####pyenv, pyvenvでできることは、両方condaでできる

```
$ conda create -n myenv
```
同じバージョンの別の環境を作れる

```
$ conda create -n mypy2env python=2
```
別のバージョンの環境を作れる

####condaとpipは共存可能
構成管理だけならMinicondaでもできる
condaになければpipでインストールすればいい

####まとめ

データサイエンスやるならAnaconda

####質疑応答
> 同じ環境を他の人のところに作る(複製する)ときは？(構成管理)

Dockerとか使うのが良い

##Talk 3:「Pythonとデータサイエンスの歴史」

柴田さん

####Pythonと科学技術計算

#####ダウンサイジング
チープ革命 -> 並列計算が主流に
ムーアの法則
Fortran, Mathematica -> Pythonに

研究者としては
- シンプルで覚えやすい
- ラーニングカーブが緩やか
-> 非エンジニア中心に流行り出した

C, C++ライブラリとの連携が容易
- NumPy, Scipy, Matplotlib

#####Numeric
NumPyの前身
sig(ワーキング・グループ)が立ち上がった
Jim Fultonがmatrixというライブラリを作成
Jim HuguninがNumericを作成

#####Jim Hugunin

* Numeric
* Jython
* IronPython
を作った -> Microsoft -> Googleに転職

#####Numeric(NumPy)のarrayとPythonのlistの違い

PurePythonのlist -> 汎用性が非常に高い反面、実効速度はやや遅い
Numericのarray -> 速い

#####無料のmatplotlib

NumPy用のビジュアライゼーションツール
Python + NumPy + matlabが商用のMATLABの代用になる

John D Hunter作
* 神経生物学者
* 2012年に癌で死亡

#####科学技術分野におけるPython

NumPy
SciPy
Matplotlib
IPython

#####Pythonが好まれる理由
######glue言語としてのPython
* コンポーネント動詞を結びつける糊(glue)として利用
* CやC++製ライブラリのラッパーとして
* GoogleでのPython導入
-> SWIG + C/C++ライブラリを活用

######マルチドメイン言語

LIGO(重力波を観測したチーム)内のPython活用分野
* 数値演算
* データ分析
* ビジュアライゼーション
* 観測機器の運転自動化
etc.

-> つまり、研究者はPythonを覚えることで、色々なことをできてしまう！

#####Pythonのエコシステムを支える資質

テスト(メンテなビリティ)
マルチプラットフォーム
ドキュメンテーション
正しい資質を標準に🌟

######NumPyの黒歴史(Numericコード汚いよね問題)

SciPyの作者Travis OliphantがNumericをリファクタリング
SciPyの一部に取り込む
NumericはSciPyの一部と誤解する人が続出
NumPyと名付ける

#####IPythonの生い立ち

高機能Pythonシェル

* コードの記述、実行、データの表示、並列化

2001年から開発
科学者用、開発当初からMathematicaやMATLABの代替として使われ始めた

🌟IPython notebookの誕生(ブラウザで動くやつ)

* Sage(セイジ)の流行

####データサイエンス

科学者
* 守備範囲が狭い
* 分析手法自体を開発
* 非ビジネス

データサイエンティスト
* 守備範囲が広い
* 既存の分析手法を組み合わせて使う
* ビジネス寄り

#####2010年頃のデータサイエンス界隈

必要に応じてツールを使い分け
分析、ビジュアライゼーションにR、MATLAB

#####2013年頃、異変が

心理学が専門のTaI Yarkoni氏曰く

2010,2011年頃
Ruby
Rails
Python
MATLAB
R

2013年頃
全部Python
+ 機械学習

#####なぜPythonの利用率が増えたか
Pythonに使えるライブラリが増えた
scikit-learn, nltk, beautifulsoup便利すぎ
🌟beautifulsoup > スクレイピングにめちゃ使える
Rubyオブジェクト指向しすぎ(頭の切り替え大変)
-> Pythonは無駄にオブジェクト指向せず、関数型も使える
Python学びやすすぎ
単一言語で仕事する利点多すぎ
-> 他の言語探索することがなくなった

#####データサイエンスをめぐる環境の変化
データ量の増大
データの質の増大
画像/動画データ

#####データサイエンスに求められる技術の変化
データベースとの接続性
並列処理による大量データの処理
大規模クライアント向け配信

#####ディープラーニングの登場🌟

Pythonの独壇場(R, MATLABとくらべて)
機械学習でも同様

#####イノベーションは必ず学問の境界領域で起こる
坂田昌一
境界領域 -> 分野と分野の間のことを指す
-> そこをデータサイエンスで結ぶ、ということ

####質疑応答

> Pythonの今後の進化については？

Python 2 -> 3にバージョンアップした際に、色々な互換性が崩れた。
こうしたことはしたくない。
ので、Pythonの言語そのものはあまり進化しない可能性が高い。
